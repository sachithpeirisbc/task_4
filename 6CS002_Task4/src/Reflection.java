
public class Reflection {
	public double enter1 = 50.5;
	private double enter2 = 25.4;
	public Reflection() {
	}
	public Reflection(double enter1, double enter2) {
	this.enter1 = enter1;
	this.enter2 = enter2;
	}
	public void sqrtenter1() {
	this.enter1 = Math.sqrt(this.enter1);
	}
	private void sqrtenter2() {
	this.enter2 = Math.sqrt(this.enter2);
	}
	public double getenter1() {
	return enter1;
	}
	private void setenter1(double enter1) {
	this.enter1 = enter1;
	}
	public double getenter2() {
	return enter2;

	}
	public void setenter2(double b) {
	this.enter2 = b;
	}
	public String toString() {
	return String.format("enter 1 value : %.2f\nenter 2 value : %.2f", enter1, enter2);
	}
	}



