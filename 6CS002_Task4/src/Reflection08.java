import java.lang.reflect.Field;
public class Reflection08 {
	public static void main(String[] args) throws Exception {
		Reflection obj = new Reflection();
		System.out.println("Reflection08");
		Field [] fields = obj.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", fields.length);
		for(Field f : fields) {
		f.setAccessible(true);
		double x = f.getDouble(obj);
		x++;
		f.setDouble(obj, x);
		System.out.printf("field name = %s, type = %s, value = %.2f\n", f.getName(), 
		f.getType(), f.getDouble(obj));
		}
		}


}
