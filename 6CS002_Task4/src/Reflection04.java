import java.lang.reflect.Field;
public class Reflection04 {
	public static void main(String[] args) throws Exception {
		Reflection obj = new Reflection();
		System.out.println("Reflection04");
		Field [] fields = obj.getClass().getFields();
		System.out.printf("There are %d fields\n", fields.length);
		for(Field f : fields) {
		System.out.printf("field name = %s,\ntype = %s, \nvalue = %.2f\n", f.getName(), 
		f.getType(), f.getDouble(obj));
		}
		}
		}

