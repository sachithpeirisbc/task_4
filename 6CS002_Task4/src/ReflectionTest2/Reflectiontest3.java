package ReflectionTest2;

import java.util.*;

public class Reflectiontest3 {
  private static List<String> check;
  private static int checks = 0;
  private static int passed = 0;
  private static int falied = 0;

  private static void TestReport(String txt) {  
    if (check == null) {
    	check = new LinkedList<String>();
    }
    check.add(String.format("check case %d: %s", checks++, txt));
  }
  public static void check1(double digit1, double digit2) {
	    if (digit1 == digit2) {
	    	TestReport(String.format("  %.2f  ==  %.2f", digit1, digit2));
	    	passed++;
	    } else {
	    	TestReport(String.format("*  %.2f  ==  %.2f", digit1, digit2));
	    	falied++;
	    }}
  public static void check2(double digit1, double digit2) {
	    if (digit1 != digit2) {
	    	TestReport(String.format("  %.2f  !=  %.2f", digit1, digit2));
	    	passed++;
	    } else {
	    	TestReport(String.format("* %.2f  !=  %.2f", digit1, digit2));
	    	falied++;
	    }} 
  public static void check3(String rate1, String rate2) {
	  try {
	        char n1 = rate1.charAt(5);
	        char n2 = rate2.charAt(5);
	        if (rate1.length() >= 5 && rate2.length() >= 5) {
	            if (Character.isDigit(n1) || Character.isDigit(n2)) {
	            	TestReport(String.format("%s and %s contain letters and numbers", rate1, rate2));
	                checks++;
	            } else if (rate1.charAt(5) == n1 && rate2.charAt(5) == n2) {
	            	TestReport(String.format("%s and %s includes the anticipated characters", rate1, rate2));
	                passed++;
	            } else {
	            	TestReport(String.format("%s and %s do not includes the anticipated characters", rate1, rate2));
	                falied++;
	            }
	        } 
	    } catch (StringIndexOutOfBoundsException e) {

	        TestReport(String.format("Both strings must be more than 5 letters"));
            
	    }
	}
  public static void check4(String digit1, String digit2) {
		try {
			char val1 = digit1.charAt(4);
			char val2 = digit2.charAt(4);
			if (digit1.charAt(4) == val1 && digit2.charAt(4) == val2) {
				TestReport(String.format("%s and %s more than 4 number of strings.", digit1, digit2));
				passed++;
		    } else {
		    	TestReport(String.format("%s and %s more than 4 number of strings.", digit1, digit2));
		    	falied++;
		    }
		}catch(StringIndexOutOfBoundsException e) {
			TestReport(String.format("Number of String must be more than 4."));
            falied++;
		}
	  }
  public static void TestReport() { 
    System.out.printf("passed = %d\n", passed);
    System.out.printf("failed : %d\n", falied);
    System.out.println();
 
    for (String t : check) {
      System.out.println(t);
    }}}
