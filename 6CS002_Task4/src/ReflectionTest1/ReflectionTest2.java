package ReflectionTest1;
import static ReflectionTest2.Reflectiontest3.*;

public class ReflectionTest2 {

  void Reflectiontest1(){
	  
	  Reflectiontest1 Reflectiontest1 = new Reflectiontest1(64.2, 82.6);
	  check1(Reflectiontest1.getdigit1(), 64.2);
	  check1(Reflectiontest1.getdigit2(), 82.6);
	  check2(Reflectiontest1.getdigit2(), 82.6);    
	  check2(Reflectiontest1.getdigit2(), 64.2);    
  }

	void Reflectiontest2(){
		
		Reflectiontest1 Reflectiontest1 = new Reflectiontest1(64.2, 82.6);
		Reflectiontest1.squaredigit1();
	    check1(Reflectiontest1.getdigit1(), 64.2);
	}
	

void Reflectiontest3(){
		Reflectiontest1 Reflectiontest1 = new Reflectiontest1(64.2, 82.6);
		  check3("ABC05", "1010");
		  Reflectiontest1 = new Reflectiontest1(64.2, 0);
		  check3("Sachith", "Hashan");
		  Reflectiontest1 = new Reflectiontest1(64.2, 0);
		  check3("N", "Y");
		  
	}

  public static void main(String[] args) {
	  
	  ReflectionTest2 test = new ReflectionTest2();
	  test.Reflectiontest1();
	  test.Reflectiontest2();
	  test.Reflectiontest3();
      TestReport();
  }
}
