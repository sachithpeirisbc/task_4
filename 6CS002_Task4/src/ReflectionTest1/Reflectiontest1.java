package ReflectionTest1;


public class Reflectiontest1 {
	
	public double digit1 = 64.2;
	private double digit2 = 82.6;
	
	public Reflectiontest1() {	
	}
	public Reflectiontest1(double digit1, double digit2) {
		this.digit1 = digit1;
		this.digit2 = digit2;
	}
	public void squaredigit1() {
		this.digit1 = Math.sqrt(this.digit1);
	}	
	private void squaredigit2() {
		this.digit2 = Math.sqrt(this.digit2);
	}	
	public double getdigit1() {
		return digit1;
	}	
	private void setdigit1(double digit1) {
		this.digit1 = digit1;
	}	
	public double getdigit2() {
		return digit2;
	}	
	public void setdigit2(double digit2) {
		this.digit2 = digit2;
	}	
	public String toString() {
		return String.format("\ndigit1 : %.2f\ndigit2 : %.2f " , digit1, digit2);
	}
}

